import * as React from 'react';
import MainScreen from '../main-screen/main-screen';

const App:React.FC = () => {
  return (
    <div className="App">
      <MainScreen />
    </div>
  );
}

export default App;
