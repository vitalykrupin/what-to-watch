export default [
  {
    id: 1,
    title: `Fantastic Beasts: The Crimes of Grindelwald`,
    imgsrc: `img/fantastic-beasts-the-crimes-of-grindelwald.jpg`
  },
  {
    id: 2,
    title: `Bohemian Rhapsody`,
    imgsrc: `img/bohemian-rhapsody.jpg`
  },
  {
    id: 3,
    title: `Macbeth`,
    imgsrc: `img/macbeth.jpg`
  },
  {
    id: 4,
    title: `Aviator`,
    imgsrc: `img/aviator.jpg`
  },
  {
    id: 5,
    title: `We need to talk about Kevin`,
    imgsrc: `img/we-need-to-talk-about-kevin.jpg`
  },
];
